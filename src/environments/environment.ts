// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBkFtEvlTV5KbY6M5EkC5SDbHAqbhuf1PI",
    authDomain: "sleepcyclemonitor.firebaseapp.com",
    projectId: "sleepcyclemonitor",
    storageBucket: "sleepcyclemonitor.appspot.com",
    messagingSenderId: "708904608513",
    appId: "1:708904608513:web:e3b1d7e21815368247f100"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
