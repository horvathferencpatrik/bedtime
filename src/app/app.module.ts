import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { Health } from '@ionic-native/health/ngx';


import { TranslateModule } from '@ngx-translate/core';
import { Sensors, TYPE_SENSOR } from '@ionic-native/sensors/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { GoogleChartsModule } from 'angular-google-charts';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, Ng2GoogleChartsModule, GoogleChartsModule, AngularFireModule.initializeApp(environment.firebaseConfig), AngularFireModule, AngularFirestoreModule, IonicModule.forRoot(), IonicStorageModule.forRoot(), AppRoutingModule,
    //    TranslateModule.forRoot({
    //      loader: {
    //        provide: TranslateLoader,
    //        useFactory: HttpLoaderFactory,
    //        deps: [HttpClient]
    //      }
    //    }), HttpClientModule
  ],
  providers: [Health, Sensors, Storage, LocalNotifications, { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }