import { Component, OnInit } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { AlertController, ToastController } from '@ionic/angular';


@Component({
  selector: 'app-alarm',
  templateUrl: './alarm.page.html',
  styleUrls: ['./alarm.page.scss'],
})
export class AlarmPage implements OnInit {


  constructor( private toastController: ToastController, private localNotification: LocalNotifications, private alertCtrl:AlertController) {}
  async ngOnInit() {
      await this.localNotification.requestPermission(); 
   }
    async alarm(){
    this.localNotification.schedule({
        title: "assd",
        id: 1,
        text: 'Single ILocalNotification',
        sound: '/assets/mp3/bensound-wonderfulsunrise.mp3',
        trigger: { at: new Date(new Date().getTime() + 1000 * 3) },
      });
  } 

  
}
