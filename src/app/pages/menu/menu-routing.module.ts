import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: 'dashboard',
    component: MenuPage,
    loadChildren: () => import('../dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'settings',
    component: MenuPage,
    loadChildren: () => import('../settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'alarm',
    component: MenuPage,
    loadChildren: () => import('../alarm/alarm.module').then( m => m.AlarmPageModule)
  },
  {
    path: 'tracker',
    component: MenuPage,
    loadChildren: () => import('../tracker/tracker.module').then( m => m.TrackerPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
