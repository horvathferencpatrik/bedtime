import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  pages = [
    {
      title: 'Dashboard',
      url: '/home/dashboard',
      icon: 'home-outline'
    },
    {
      title: 'Tracker',
      url: '/home/tracker'
    },
    {
      title: 'Settings',
      url: '/home/settings'
    }
  ]
  selectedPath: string="";

  constructor(private router: Router, private auth: AngularFireAuth) { 
    this.router.events.subscribe((event: RouterEvent)=>{
      this.selectedPath = event.url;
    });
  }
  ngOnInit() {
  }
  logout(){
    this.auth.signOut().then(()=> this.router.navigate(['/login']));
  }
}
