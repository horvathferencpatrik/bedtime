import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {auth} from 'firebase/firebase-auth';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email: string="";
  password: string="";
  rememberMe: boolean;
  constructor(public afAuth: AngularFireAuth,private storage: Storage, private route: Router, public toastController: ToastController) {
    this.storage.get('rememberMe').then((val) => {
      if(val=="yes"){
        this.rememberMe=true;
        this.storage.get('email').then((val) => {
          this.email=val;
          })
      }
      })
  }

  ngOnInit(){
    this.storage.get('night').then((val) => {
    if(val=="yes"){
      document.body.setAttribute('color-theme','dark');
    }
    })

  }
  async login(){
    const{ email, password , rememberMe}=this
    if(rememberMe){
      console.log("remember");
      this.storage.set('rememberMe','yes');
      this.storage.set('email',this.email);
    }else{
      console.log("remember2");
      this.storage.set('rememberMe','no');
      this.storage.set('email',"");
    }
    if(password=="" || email==""){
      const toast = await this.toastController.create({
        message: 'Please fil in the fields',
        duration: 2000
      });
      toast.present();
    }else{
      try{
          const res = await this.afAuth.signInWithEmailAndPassword(email, password).then(()=> this.route.navigate(['/home/dashboard']));
          this.email="";
          this.password="";
      }catch(err){
        console.dir(err)
        const toast = await this.toastController.create({
          message: 'Login failed',
          duration: 2000
        });
        toast.present();
      }
    }
  }
  forgotPassword(){
    this.route.navigate(['/forgot-password']);
  }
  register(){
    this.route.navigate(['/register']);
  }
}
