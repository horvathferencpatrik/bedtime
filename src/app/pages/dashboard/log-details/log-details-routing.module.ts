import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogDetailsPage } from './log-details.page';

const routes: Routes = [
  {
    path: '',
    component: LogDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LogDetailsPageRoutingModule {}
