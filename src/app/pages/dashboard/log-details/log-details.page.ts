import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { ChartDataset } from 'chart.js';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-log-details',
  templateUrl: './log-details.page.html',
  styleUrls: ['./log-details.page.scss'],
})
export class LogDetailsPage {

  db = firebase.default.firestore();
  email = firebase.default.auth().currentUser.email;
  url: string;
  id: string[];
  log: any;
  dates: any[];
  max: any;

  i: number;
  dataHabits: { labels: string[]; datasets: { label: string; data: number[]; backgroundColor: string; }[]; };
  constructor(private router: Router, private http: HttpClient) { }

  async ngOnInit() {
    this.url = this.router.url;
    this.id = this.url.split('/');
    console.log(this.id[3]);
    const ref = this.db.collection('Logs').doc(this.id[3]);
    this.log = await (await ref.get()).data();
    this.dates = this.log.dates
    const len = this.dates.length;
    const startDate = this.dates[0];
    console.log("End date: " + this.log.endDate);
    const diff = (this.log.endDate - startDate);
    console.log(diff);
    this.max = 0;
    console.log(this.max);
    this.data = {
      labels: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
      datasets: [{
        label: "Sleep Data",
        data: [this.dates[0], this.dates[1], this.dates[2], this.dates[3], this.dates[4], this.dates[5], this.dates[6], this.dates[7], this.dates[8], this.dates[9], this.dates[10], this.dates[11], this.dates[12], this.dates[13], this.dates[14], this.dates[15]],
        backgroundColor: "#f38b4a",
      }]
    };
    this.dataHabits = {
      labels: ["cigarettes", "tolilets", "coffees", "drinks"],
      datasets: [{
        label: "Number of",
        data: [this.log.cigarette, this.log.toilet, this.log.coffee, this.log.drink],
        backgroundColor: "#f38b4a",
      }]
    };
  }

  data: any;
  bar: any;
  //Line Chart
  typeLine = 'line';
  type = 'bar';
  options = {
    responsive: true,
    maintainAspectRatio: true,
    scales: {
      yAxes: [{
        ticks: {
          min: 0
        }
      }]
    }
  };
  //Bar Chart
  options2 = {
    responsive: true,
    maintainAspectRatio: true,
    scales: {
      yAxes: [{
        ticks: {
          max: 10,
          min: 0
        }
      }]
    }
  };

}
