import { Component, OnInit } from '@angular/core';
import { Health } from '@ionic-native/health/ngx';
import { Log } from '../tracker/tracked.model';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase2 from 'firebase/app';
import 'firebase/firestore';
import { AlertController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  logs=[];
  email = firebase.default.auth().currentUser.email;
  db=firebase2.firestore(); 
  empty=false;
  sleep:any = [];
  sleep2:any;
  dateForm:boolean;
  loaded = false;

  constructor(public health: Health,private storage:Storage, private afstore: AngularFirestore, public loadingController: LoadingController,public alertController:AlertController) {
    this.storage.get('date').then((val) => {
      if(val=="yes"){
        this.dateForm=true;
      }else{
        this.dateForm=false;
      }
      })
   } 

   loadSleepData(){
    this.sleep2=this.health.query({
      startDate: new Date(new Date().getTime() - 4 * 24 * 60 * 60 * 1000), // three days ago
      endDate: new Date(), // now
      dataType: 'sleep',
      limit: 400
    })
   }
  async ngOnInit() {
    const loading = await this.loadingController.create({
      cssClass: 'loadingController',
      message: 'Please wait...',
      duration: 1000
    });
    await loading.present();
    
    const snapshot = await this.db.collection('Logs').where("uid","==",this.email).orderBy("endDate").get();
    snapshot.forEach((doc) => {
    console.log(doc.id, '=>', doc.data());
    let log: Log ={
      uid:doc.data().uid,
      startDay: doc.data().startDay,
      endDay:doc.data().endDay,
      startMonthIndex: doc.data().startMonthIndex,
      endMonthIndex: doc.data().endMonthIndex,
      startHour: doc.data().startHour,
      endHour: doc.data().endHour,
      startMinute: doc.data().startMinute,
      endMinute: doc.data().endMinute,
      diff: doc.data().diff,
      docId: doc.id,
      endDate: new Date(doc.data().endDate)
    };
    this.logs.push(log);
  });
  this.logs=this.logs.reverse();
  if(this.logs.length==0){
    this.empty=true;
    this.loaded=true;
  }

  }
  async delete(docId){
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        message: 'Are you sure want to delete it?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
            }
          }, {
            text: 'Okay',
            handler: () => {
              this.db.collection('Logs').doc(docId).delete();
              this.logs=[];
              this.ngOnInit();
            }
          }
        ]
      });
  
      await alert.present();
    
    
    }

  
}


