import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(public afAuth: AngularFireAuth, public toastController: ToastController, private route: Router) { }
    email:string = "" ;
    password:string = "" ;
    passwordAgain:string = "" ;
  ngOnInit() {
  }
  async register(){
    const{ email, password, passwordAgain } = this;
    if( email=="" || password=="" ||  passwordAgain=="" ){
      const toast = await this.toastController.create({
        message: 'Please fill in the fields',
        duration: 2000
      });
      toast.present();
    }else if(password!=passwordAgain){
      const toast = await this.toastController.create({
        message: 'The given passwords are different',
        duration: 2000
      });
      toast.present();
    }else if(password.length<6){
      const toast = await this.toastController.create({
        message: 'The password must be at least 6 characters',
        duration: 2000
      });
      toast.present();
    }else{
      try{
        const res= await (await this.afAuth.createUserWithEmailAndPassword(email,password));
        (await this.afAuth.currentUser).sendEmailVerification()
        const toast = await this.toastController.create({
          message: 'Successful registration',
          duration: 2000
        });
        toast.present();
        this.route.navigate(['/login']);
      }catch(err){
        console.dir(err)
        const toast = await this.toastController.create({
          message: 'Unknown error',
          duration: 2000
        });
        toast.present();
      }
    }
  }
}
