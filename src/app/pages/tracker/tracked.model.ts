import { Timestamp } from "@google-cloud/firestore";

export interface Log{
    startDay: number,
    startHour: number,
    startMinute: number,
    startMonthIndex: number,
    endDay: number,
    endHour: number,
    endMinute: number,
    endMonthIndex: number,
    diff: String,
    uid: string,
    docId: string,
    endDate: Date
}