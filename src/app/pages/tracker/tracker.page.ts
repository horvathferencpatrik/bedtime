import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Sensors } from '@ionic-native/sensors/ngx';
import { AlertController, LoadingController, Platform, ToastController } from '@ionic/angular';
import * as firebase from 'firebase/app';
import { Howl } from 'howler';

@Component({
  selector: 'app-tracker',
  templateUrl: './tracker.page.html',
  styleUrls: ['./tracker.page.scss'],
})
export class TrackerPage implements OnInit {
  firebase: any;
  lux: any;
  x = 0;
  y = 0;
  z = 0;
  isRecording = false;
  dates: any[];
  counter: number;
  trackedDate: Date;
  constructor(private sensors: Sensors, private loadingController: LoadingController, platform: Platform, private afstore: AngularFirestore, private toastController: ToastController, private alertController: AlertController) {
    platform.ready().then(() => {
      this.initSensor();
    })
    // platform2.ready().then(() => {
    //   this.initSensor2();
    // })  
  }
  initSensor() {
    this.sensors.enableSensor("GYROSCOPE");
    setInterval(() => {
      this.sensors.getState().then((values) => {
        this.x = values[0] * 1000;
        this.y = values[1] * 1000;
        this.z = values[2] * 1000;
        if ((this.x > 1.60 || this.y > 1.60 || this.z > 3.2 || this.x < -3.5 || this.y < -3.50 || this.z < -3.2) && this.isRecording) {
          if (new Date().valueOf() > this.trackedDate.valueOf()) {
            this.dates.push(new Date().valueOf());
            this.counter++;
            this.trackedDate = new Date(new Date().valueOf() + new Date(25 * 1000).valueOf());
          }
        }
      });
    }, 1000)

  }
  // initSensor2(){
  //        this.sensors2.enableSensor("LIGHT");
  //   setInterval(() => {
  //     this.sensors2.getState().then((values2) => {
  //       this.lux = values2[0];
  //       if(values2[0]<11){
  //         alert(values2)
  //       }
  //     });
  //   }, 1000)

  // }

  musics = [
    {
      title: 'Storm',
      path: '/assets/mp3/storm.mp3',
      tumbnail: '/assets/tumbnails/storm.jpeg'
    },
    {
      title: 'Bridge',
      path: '/assets/mp3/NightInTheCity.mp3',
      tumbnail: '/assets/tumbnails/NightInTheCity.jpg'
    },
    {
      title: 'Next To The River',
      path: '/assets/mp3/NextToTheRiver.mp3',
      tumbnail: '/assets/tumbnails/NextToTheRiver.jpeg'
    }
  ];

  activeMusic = null;
  player: Howl = null;
  isPlaying = false;
  isStarted = false;
  startDate = null;
  startedAt = null;
  endDate: Date;
  email = firebase.default.auth().currentUser.email;

  async ngOnInit() {
    const loading = await this.loadingController.create({
      cssClass: 'loadingController',
      message: 'Please wait...',
      duration: 2000
    })
    await loading.present();

  }
  startSleep() {
    this.trackedDate = new Date(new Date().valueOf() + new Date(13000).valueOf());
    this.dates = [];
    this.counter = 0;
    this.isStarted = true;
    this.startDate = new Date();

    const day = this.startDate.getDate();
    const monthIndex = this.startDate.getMonth();
    const minutes = this.startDate.getMinutes();
    const hours = this.startDate.getHours();
    this.startedAt = (monthIndex + 1) + "/" + day + " " + hours + ":" + minutes;

    this.presentToast("Good night!", 1500);
    this.delay(10000);
    this.isRecording = true;


  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async presentToast(message: string, duration: number) {
    const toast = await this.toastController.create({
      message,
      duration
    });
    toast.present();
  }
  // loop(){
  //   this.sensors.enableSensor("GYROSCOPE");
  //   setInterval(() => {
  //     this.sensors.getState().then((values) => {
  //       this.lux = values;
  //       // if(values[0]< -0.1 || values[0]> 0.08){
  //         alert(values)
  //       // }
  //     });
  //   }, 300);
  //   this.loop();
  // }


  async stopSleep() {
    this.isRecording = false;
    if (this.isPlaying) {
      this.player.stop();
    }
    this.presentToast("Good morning!", 1500);

    this.isStarted = false;
    this.endDate = new Date();
    this.activeMusic = null;
    const diff = new Date(this.endDate.valueOf() - this.startDate.valueOf()).getHours() - 1 + "h :" + new Date(this.endDate.valueOf() - this.startDate.valueOf()).getMinutes() + "m";
    console.log("Eltelt idő:" + diff);
    const diff2 = this.endDate.valueOf() - this.startDate.valueOf();
    var statDates = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.dates.map((data) => {
      if (data <= (this.startDate.valueOf() + diff2 / 16)) {
        statDates[0]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 2) / 16)) {
        statDates[1]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 3) / 16)) {
        statDates[2]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 4) / 16)) {
        statDates[3]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 5) / 16)) {
        statDates[4]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 6) / 16)) {
        statDates[5]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 7) / 16)) {
        statDates[6]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 8) / 16)) {
        statDates[7]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 9) / 16)) {
        statDates[8]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 10) / 16)) {
        statDates[9]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 11) / 16)) {
        statDates[10]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 12) / 16)) {
        statDates[11]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 13) / 16)) {
        statDates[12]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 14) / 16)) {
        statDates[13]++;
      } else if (data <= (this.startDate.valueOf() + (diff2 * 15) / 16)) {
        statDates[14]++;
      } else {
        statDates[15]++;
      }
    });

    const alert = await this.alertController.create({
      cssClass: 'lastNight',
      header: 'How was your last night?',
      inputs: [
        {
          name: 'coffee',
          id: 'coffee',
          type: 'number',
          placeholder: 'coffee'
        },
        {
          name: 'toilet',
          type: 'number',
          id: 'toilet',
          placeholder: 'Toilet'
        }, {
          name: 'cigarette',
          type: 'number',
          id: 'cigarette',
          placeholder: 'cigarette'
        }, {
          name: 'drink',
          type: 'number',
          id: 'drink',
          placeholder: 'drink'
        }
      ],
      buttons: [
        {
          text: 'Ok',
          handler: (data) => {
            this.afstore.collection('Logs').add({
              uid: this.email,
              startDay: this.startDate.getDate(),
              endDay: this.endDate.getDate(),
              startMonthIndex: (this.startDate.getMonth() + 1),
              endMonthIndex: (this.endDate.getMonth() + 1),
              startHour: this.startDate.getHours(),
              endHour: this.endDate.getHours(),
              startMinute: this.startDate.getMinutes(),
              endMinute: this.endDate.getMinutes(),
              diff: diff,
              docId: 0,
              endDate: this.endDate.valueOf(),
              startDate: this.startDate.valueOf(),
              coffee: data.coffee,
              toilet: data.toilet,
              cigarette: data.cigarette,
              drink: data.drink,
              dates: statDates,
              counter: this.counter
            });
          }

        }
      ]

    });
    await alert.present();
  }

  start(music: any) {
    if (this.isPlaying) {
      this.player.stop();
    }
    this.player = new Howl({
      src: [music.path],
      loop: true,
      volume: 0.3
    });
    this.player.play();
    this.activeMusic = music
    this.isPlaying = true
  }
  pause() {
    if (this.player) {
      this.player.pause();
      this.activeMusic = null;
    }
  }
  ngOnDestroy() {
    if (this.isPlaying) {
      this.player.stop();
    }
  }
}
