import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  email: string = "" ;
  constructor(public afAuth: AngularFireAuth, public toastController: ToastController, private route: Router) { 
  }

  ngOnInit() {
  }
  async sendNewPassword(){
    const {email} = this;
    this.afAuth.sendPasswordResetEmail( this.email )
      .then(data => {
        console.log(data);
        this.presentToast( 'Password reset email has been sent' ,  'bottom' , 1000);
        this.route.navigateByUrl( '/login' );
      }).catch(err => {
        this.presentToast( `${err}` ,  'bottom' , 1000 );
      });
  }
  async presentToast(message, position, duration) {
    const toast = await this.toastController.create({
      message,
      duration,
      position
    });
    toast.present();
  }

}
