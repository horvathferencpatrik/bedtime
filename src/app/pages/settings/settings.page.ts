import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  isNight:boolean;
  dateForm:String;
  constructor(private storage: Storage) { }

  ngOnInit() {
    this.storage.get('night').then((val) => {
      if(val=="yes"){
        this.isNight=true
      }
      })
      this.storage.get('date').then((val) => {
        if(val=="yes"){
          this.dateForm="MM-DD";
        }else{
          this.dateForm="DD-MM";
        }
        })
  }

  toggleTheme(event){

    if(!this.isNight){
      document.body.setAttribute('color-theme','dark');
      this.storage.set('night', 'yes');
      this.isNight=true;
      }else{
      document.body.setAttribute('color-theme','ligth');
      this.storage.set('night', 'no');
      this.isNight=false;
    }
  }
  showSelectValue(value){
    if(value=="YMD"){
      this.storage.set('date','yes');
    }else if(value=="DMY"){
      this.storage.set('date','no');
    }
  }
}
